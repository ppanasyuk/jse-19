package ru.t1.panasyuk.tm.api.repository;

import ru.t1.panasyuk.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}