package ru.t1.panasyuk.tm.api.repository;

import ru.t1.panasyuk.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {
}