package ru.t1.panasyuk.tm.api.service;

import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task>, ITaskRepository {

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task create(final String name, final String description);

    Task create(String name);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}