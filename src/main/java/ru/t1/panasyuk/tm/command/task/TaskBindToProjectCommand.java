package ru.t1.panasyuk.tm.command.task;

import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Bind task to project.";

    private static final String NAME = "task-bind-to-project";

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(projectId, taskId);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}