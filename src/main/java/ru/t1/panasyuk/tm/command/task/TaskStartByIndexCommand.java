package ru.t1.panasyuk.tm.command.task;

import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Start task by index.";

    private static final String NAME = "task-start-by-index";

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}