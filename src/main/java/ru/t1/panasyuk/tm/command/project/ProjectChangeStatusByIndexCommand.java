package ru.t1.panasyuk.tm.command.project;

import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "Change project status by index.";

    private static final String NAME = "project-change-status-by-index";

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Status.getDisplayNames());
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getProjectService().changeProjectStatusByIndex(index, status);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}